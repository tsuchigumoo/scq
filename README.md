# Stupid Cybersecurity Questions

# What is SCQ

SCQ is a repository containing tons of theoretical cybersecurity questions an employer may ask you. Few questions are interesting, others will not really be usefull for you work and some are just stupid questions.
The questions are stored in md files and anki decks (recommended for learning) with short answers, pictures and links to youtube videos.

# How to use?
You can just view the md files containing the questions and answers in hte questions folder, or use the anki decks with anki.

# How to ask the most stupid questions?

Just act like every interviewer asking these questions, take the top 10, top 25 or top 50 "best security interview questions" on some random blogpost or youtube video, and add these rules:

1. Asking what information is behind a number

- Example: Asking what thing is behind a port number, " what is behind the port number 21? is it using TCP or UDP?" "what does the number 23 means to you?"

- Replying "I am sorry but I use the internet for my job and I am not gonna learn all the port numbers, even good pentesters use google" is not going to work for these people unfortunately.

2. Asking something that is unrelated to the job

- Example: Asking almost only network, cryptography and common stupid questions for a DevSec job.

3. Asking something that is really old/patched and not used anymore.

- Example: "Explain how to perform buffer overflows, whithout ASLR".

4. Asking very broad question without much context, and expecting a monologue.

- Example: "How would you secure a software?"

5. Asking very specific things only few people know the answer

- Example: "What is the content of the RFC xxxx?" "Explain how to setup a stack canary" "What is the name behind the linux firewall?"

6. Asking " how would you do this" and waiting for a very specific answer

- Example: " How would you read a file on linux?" and waiting for an answer such as "I would use cat" and rejecting answers such as "I would use less"
I even had an interviewer telling me for this answer " This answer is not written on my interview question paper :( "

7. Asking questions that could be totally different depending on the context.

- Example: "Asking how many bits are contained in a byte" The interviewer will almost always expect 8, but in reality the number of bits in a "byte"  will vary depending of your cpu architecture.

 8. Asking questions with the wrong words

- Example :  "Explain SSL and how is it used"
SSL is dead and not used anymore. Long live TLS or SSL/TLS.

 9. Asking questions when the interviewer thinks he/she knows the answer but almost everyone does not know the correct answer.

- "Would you compress or encrypt first?"
90% of the time they will expect you to say "compress first", which is a wrong for good security practices. The correct answer is neither, because if you compress first you can leak information.

# WHY SCQ?

I got fed up with stupid questions some employers asked me, that were totally unrelated to the job. Some asked me to explain the content of the RFC 1918 without being allowed to use google, and the difference between a hub and a switch and I got mad because I know what a RFC is and I have never used or seen a hub in my entire life, and as a selft-taugh and OSCP holder I learned everything in the field, not in books from the 90s with things I will never use.

# FIELDS

### "Basic" and most common Cyber Security Questions
### Encryption and Authentication
### Network and Logs
### Application security and web
### Mobile Application Security
### Cloud and container Security
### Hardware
### SOC
### Pentesting
### Compliance / Governance
### Tools, lab and Games


# To DO

Anki decks for each field
links on readme / markdown
