# Tools, lab and Games
   * Have I played CTF?
   * Would you decrypt a steganography image?
   * You're given an ip-based phone and asked me to decrypt the message in the phone.
   * What CND tools do you knowledge or experience with?
   * What is the difference between nmap -ss and nmap -st?
   * How would you filter xyz in Wireshark?
   * Given a sample packet capture - Identify the protocol, the traffic, and the likelihood of malicious intent.
   * If left alone in office with access to a computer, how would you exploit it?
   * How do you fingerprint an iPhone so you can monitor it even after wiping it?
   * How would you use CI/CD to improve security?
   * You have a pipeline for Docker images. How would you design everything to ensure the proper security checks?
   * How would you create a secret storage system?
   * What technical skill or project are you working on for fun in your free time?
   * How would you harden your work laptop if you needed it at Defcon?
   * If you had to set up supply chain attack prevention, how would you do that?

