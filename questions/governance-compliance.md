# Compliance / Governance

* What is Information Security Governance?
     IT security governance is the system by which an organization directs and controls IT security (adapted from ISO 38500).
     Governance specifies the accountability framework and provides oversight to ensure that risks are adequately mitigated, while management ensures that controls are implemented to mitigate risks.

   * What is Incident Response ?
   * Can you explain SOC 2?
   * What are the five trust criteria?
   * How is ISO27001 different?
   * Can you list examples of controls these frameworks require?
   * What is the difference between Governance, Risk and Compliance?
   * What does Zero Trust mean?
   * What is role-based access control (RBAC) and why is it covered by compliance frameworks?
   * What is the NIST framework and why is it influential?
   * What is the OSI model?
   * What is DLP ? Heard of it ?
   3. What is non-repudiation (as it applies to IT security)?
   * Non-repudiation is the assurance that someone cannot deny the validity of something.
    * Non-repudiation is a legal concept that is widely used in information security and refers to a service, which provides proof of the origin of data and the integrity of the data.
    * The concept that every service or process should provide proof of the integrity (such as a signature) and origin of data. It is a aspect of information security because it ensures the integrity of data, processes, and transmissions

* What is the relationship between information security and data availability?
  Information security encompasses the tactics and processes used to protect data and ensure that only authenticated and approved users have access to authorized data. Information security cannot inhibit authorized users from access, but it must protect data from unauthorized access or theft using logical and physical controls.

* What is a security policy and why do we need one?
    A document that governs how, when, and why users may access and interact with information systems. It provides users with reasonable expectations of their roles and responsibilities, as well as serves as a guidance for procedural actions.

* What is the difference between logical and physical security? Can you give an example of both?
    Protecting the people involves a combination of physical and logical security. Physical security keeps them safe by allowing only authorized individuals into the building. Logical security protects their computers and data from unauthorized access.
     Logical security protects computer software by discouraging user access by implementing user identifications, passwords, authentication,and biometrics. Physical security prevents and discourages attackers from entering a building by installing fences, alarms, cameras, security guards, electronic access control, intrusion detection and administration access controls. The difference between logical security and physical security is logical security protects access to computer systems and physical security protects the site and everything located within the site.
* Is there an acceptable level of risk?
     A level of risk mitigates as much risk as possible while still enabling the business to operate at optimum levels.
The risk assignment should always be re-evaluated as new technologies, processes, and the threat environment changes.
* How do you measure risk? Can you give an example of a specific metric that measures information security risk?
     Risk is calculated by multiplying the threat likelihood value by the impact value, and the risks are categorized as high, medium or low based on the result.  ISO Standard 27005 (Information Security Risk Management - ISRM)
* Can you give me an example of risk trade-offs (e.g. risk vs cost)?
* What are the most common types of attack that threaten enterprise data security?
    * Insider Threats
    * Ransomware
    * Malware that enables data exfiltration,
    * Scripting attacks (XSS, cross site forgery)
    * Injection Attacks (SQL/ javascript)
* What is the difference between a threat and a vulnerability?
     A vulnerability is weakness in a system, site, or process that a threat may take advantage of. A threat is any entity or process that manipulates a weaknesses to commit unauthorized actions or attain unauthorized access to an information system.
* What is a security control?
     A safeguards or countermeasure to avoid, detect, counteract, or minimize security risks.
* What are the different types of security control?
     Logical Security Controls
     Physical Security Controls
     Procedural Security Controls
* Can you describe the information lifecycle? How do you ensure information security at each phase?
    It is the way that information is properly managed throughout its life cycle in order to get the full use and benefit from it. Includes Plan, Obtain, Store/Share, Maintain, Apply, and Dispose
* Are you familiar with any security management frameworks such as ISO/IEC 27002?
     ISO/IEC 270002
     NIST SP 800-53: Nist Cyber Security framework
     SOC 2/3
     PCI-DSS
     HIPAA
     NERC-CIP
    FFIEC
   * What is captured in a security assessment plan (security test plan)?
     The security assessment plan documents the controls and control enhancements to be assessed, based on the purpose of the assessment and the implemented controls identified and described in the system security plan. The security assessment plan defines the scope of the assessment, in particular indicating whether a complete or partial assessment will be performed and if the assessment is intended to support initial pre-authorization activities associated with a new or significantly changed system or ongoing assessment used for operational systems. The security assessment plan also describes the procedures to be used for each control—typically drawn from Special Publication 800-53A or from available assessment cases and tailored as necessary to satisfy organizational or system-specific requirements—including the selection of assessment methods and objects and assigned depth and coverage attributes.
   * What special security challenges does Service-Oriented-Architecture (SOA) present?



