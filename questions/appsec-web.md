# Application security and web

   * Differentiate XSS from CSRF.
   * What do you do if a user brings you a pc that is acting 'weird'? You suspect malware.
   * What is the difference between tcp dump and FWmonitor?
   * Do you know what XXE is?
   * Explain man-in-the-middle attacks.
   * What is a Server Side Request Forgery attack?
   * Describe what are egghunters and their use in exploit development.
   * How is pad lock icon in browser generated?
   * What is Same Origin Policy and CORS?

* Here is a regex, tell me what it does
* Code review a project and look for the vulnerability.
* How would you conduct a security code review?
* How can Github webhooks be used in a malicious way?
* If I hand you a repo of source code to security audit what’s the first few things you would do?
* Can I write a tool that would search our Github repos for secrets, keys, etc.?
* Given a CVE, walk us through it and how the solution works.
* Tell me about a repetitive task at work that you automated away.
* How would you analyze a suspicious email link?
* Heard of OWASP ? What is it ? name some Vulnerabilities from OWASP-T10.
* How do you handle Brute Forcing on your application ?
* What is Authentication and Authorization ?
* What is Steteful and Steteless in HTTP context ?
* How does HTTP handles state ?
* What is Cross Site Scripting ?
* What is difference in stored , reflected, and DOM XSS ?
* Which of the XSS attacks are hard to detetct and why ?
* What is the defense against XSS ? Remidiation.
* Do you prefer black-listing approach or whitelisting approach ? and Why ?
* What is CSRF ? Impact ? and Remidiation ?
* When investigating CSRF Attack , wat are the things you will look for ?
* Can you perform CSRF attack if HTTP method is PUT considering there is no CSRF Prevention, Explain?
* How do you determine if the Website is hosted on IIS or Apache or Nginix or whatever server stack ?
* What is SQL Injection ?
* Name some Types of SQL Injection Vulnerability.
* Explain Union Based SQL Injection.
* Explain Time Based SQL Injection.
* Explain Blind SQL Injection.
* How do you protect against SQLi ?
* What is Prepared Statements and Paramatrized Query ? (in Context of SQLi)
* What is 2nd-Order-SQLi ?
* How do you store password for applications in database ?
* What is RCE ? How do you test for RCE ? How can this bug be remidiated ?
* Explain OS Command Injection .
* What is CORS ? and SOP ?
* Does CORS protect against CSRF Attack ?
 Explain XXE ? What causes this flaw ? How do you mitigate it ?
* What are some Security headers in HTTP Request? Name some.
* Mention some HTTP Response Headers for Security ? Explain.
* What are various HTTP methods ?
* What is difference in GET POST and PUT Request ?
* What is CSP (Content Security Policy) ?
* Explain Race Condition ? How can you test for it ?
* Explain Cookie Attributes/Flags ? and Explain.
* What is Threat Modeling ?
* When do you interact with developers for security testing ?
* Are you aware of the Software Development Life Cycle ?
* When in SDLC should you engage with Developers ?
* What is CI/CD Pipeline ?  Explain the role of this with the context of Security.
* Classify some Web Vulnerabilities into Low, Medium , High and Critical category. Reason why !
* Known that MD5 is not the most secured hasing Algorithm, Why we dont use SHA256 or others always ?
* Internet facing NGINIX is being used in front of multiple applications (micro service architecture). These application are accessible to users via different sub-domains through NGINIX, What can go Wrong ?
* Can server SSL Certificate prevent SSL Injection against your system ? Explain.
* An Attacker is trying to extract session cookie using XSS Vulnerability, but a blank popup is shown. What could be the reason for this behaviour ?
* Web Application allows user to download their account statement in DF format. How can you securely implement this functionality ? Explain.
* What is Threat Model / Threat Modeling ?
* What is STRIDE ?

* Can you briefly discuss the role of information security in each phase of the software development lifecycle?
    * Requirements Gathering
      * Security Requirements
      * Setting up Phase Gates
      * Risk Assessment
    * Design
      * Identify Design Requirements from security perspective
      * Architecture & Design Reviews
      * Threat Modeling
    * Coding
      * Coding Best Practices
      * Perform Static Analysis
    * Testing
      * Vulnerability Assessment
      * Fuzzing
    * Deployment
      * Server Configuration Review
      * Network Configuration Review


