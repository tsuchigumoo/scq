   * How many packets are sent and received in 3-way handshake ?
   * Explain BruteForce Attack . How do you detect it ?
         Account Lockouts/timeouts
         API rate limiting
         IP restrictions
         Fail2ban

   * What are the differences between a hub, switch and a router?
   * How can you prevent Brute Force attack ? Mention some methods.
   * How SSL Certificate Exchange happens ?
   * What is a DMZ?
   * What do you understand by DMZ and Non-DMZ ?
   * What is Meta Data and how can you view it ? What Risk it causes ?
   * How can you check for Data Exfiltration Activities ?
   * How do you place a firewall, load balancer, proxy ? in what order and why ?
   * What information can you get from MAC Address ?
   * What port does PING works on ? ( I will change this Ping thing, too much resued now)
   * What is SSH ? on What port does SSH works ?
   * Can you do SSH from Windows ?
   * What is a Load Balancer?
   * What is CDN ?
   * Can you explain man-in-the middle attack?
   * Does HTTPS/SSL protects from Man-in-the-Middle Attack ?
   * What is difference in IPS and IDS ?
   * How is TCP/IP Layer Different from OSI Layers in Networking?
   * Do you prefer filtered ports or closed ports on your firewall?
   * What is a firewall? What are different types of Firewall ?
   * How can you bypass firewall? or IDS ?
   * What is Fragmentation attack ?
   * How can Fragmentation be used as DoS Attack ? How can this be avoided or handled ?
   * Besides firewalls, what other devices are used to enforce network boundaries?
   * What is a honeypot?
   * What is the difference between an HIDS and a NIDS? Exmaples of both.
   * What is worse in detection, a false negative or a false positive? And why?
   * What is DDoS and DoS attack ?
   * What do you understand by IP Subnetting ?
   * Explain NAT (Network Address Translation) ?
   * What is Port Forwarding ? and how/why it is used ?
   * What is VLAN ?
   * What Security Principle means a signed message came from the owner of key that signed it ? (non-repundiation, Integrity, authority, -non-verifiabilit)
   * What is ARP Poisoning ?
   * What are common ports involving security, what are the risks and mitigations?
   * What is DNS ? How DNS Resolution happens ? is it over TCP or UDP ?

   * DNS Communication Happens on which port ?
   * What is Proxy
   * What is Forward Proxy and Reverse Proxy?
   * What is the Difference in VPN and Proxy ?
   * Describe HTTPs and how it is used.
   * What is the difference between HTTPS and SSL?
   * How does threat modeling work?
   * What is a subnet and how is it useful in security?
   * What is subnet mask?
   * Explain what traceroute is.
   * Draw a network, then expect them to raise an issue and have to figure out where it happened.
   * Explain TCP/IP concepts.
   * What is OSI model?
   * How does a router differ from a switch?
   * Describe the Risk Management Framework process and a project where you successfully implemented compliance with RMF.
   * How does a packet travel between two hosts connected in same network?
   * Explain the difference between TCP and UDP.
   * Which is more secure and why?
   * What is the TCP three way handshake?
   * What is the difference between IPSEC Phase 1 and Phase 2?
   * What are biggest AWS security vulnerabilities?
   * How do web certificates for HTTPS work?
   * What is the purpose of TLS?
   * What is the difference in SSL and TLS ?

   * Is ARP UDP or TCP?
   * Explain what information is added to a packet at each stop of the 7 layer OSI model.
   * Walk through a whiteboard scenario for your environment of choice (Win/Linux) in which compromising the network is the goal without use of social engineering techniques (phishing for credential harvesting, etc).
   * Explain how you would build a web site that could secure communications between a client and a server and allow an authorized user to read the communications securely.
   * How does an active directory work?
   * Do you know how Single Sign-On works?
   * What is a firewall?
   * How does it work?
   * How does it work in cloud computing?
   * Difference between IPS and IDS?
   * How do you build a tool to protect the entire Apple infra?
   * How do you harden a system?
   * How to you elevate permissions?
   * Describe the hardening measures you've put on your home network.
   * What is traceroute? Explain it in details.
   * How does HTTPS work?
   * What would you do if you discovered an infected host?
   * What is SYN/ACK and how does it work?
   * You got the memory dump of a potentially compromised system, how are you going to approach its analysis?
   * How would you detect a DDOS attack?
   * How does the kernel know which function to call for the user?
   * How would you go about reverse-engineering a custom protocol packet?
   * Can you give me an overview of IP multicast?
     IP multicast is a method of sending Internet Protocol (IP) datagrams to a group of interested receivers in a single transmission. It is the IP-specific form of multicast and is used for streaming media and other network applications. It uses specially reserved multicast address blocks in IPv4 and IPv6.
