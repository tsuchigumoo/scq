### Mobile Application Pentesting
* What are some common Risks in Mobile Applications ?
* Describe Programatic ways to detect if iOS or Android device is jailbroken or rooted.
* Can SMS be used as a medium to perform SQL Injection on Android Application. Explain ?
* Which tool is (mostly*) used to hook into iOS application
* Which protection mechanism is used for distributing Apple iOS Application on iTunes store?
* What are different Obfuscators used to Protect Mobile Apps ?
* What are different ways for Mobile Application to store and Protect sensative data in Android and iOS. Recomend best practices.
* Brief about the Security improvements in Recent (last 2) Android Releases.
* Mention different steps you would perform doing reverse engineering on an  iOS Application downloaded from iTunes Store.
* Consider that you have decompiled a Android Application, made changes to the code and apk design, Will you be able to install this repacked APK on a newly formatted Android device ? Why ? or Not ?
* Provide ADB command with example to fetch APK file from Android Device.
* Can Adnroid malware App Extract sqlite file of another app? How? Why?or Not ? Explain with any assumptions made .
* Explain different approaches of bypassing SSL Pinning in Android and iOS Applications.

