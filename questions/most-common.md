# "Basic" and most common Cyber Security Questions

* Explain what traceroute is.
- Traceroute is a computer network diagnostic commands for displaying possible routes (paths) and measuring transit delays of packets across an Internet Protocol (IP) network by sending ICMP packets.

* What is the difference between an IDS and IPS
- An IDS is designed to only provide an alert about a potential incident, An IPS, on the other hand, takes action itself to block the attempted intrusion or otherwise remediate the incident.

* What is a brute force attack and how do you prevent one?
- Brute force attacks  are a type of cyberattack that involves trying different variations of symbols or words until you guess the correct password.

*  What is a Firewall?
- A firewall is a network security system that monitors and controls incoming and outgoing network traffic  based on  security rules.

*  How to reset a password-protected BIOS configuration?
- Removing the CMOS Battery or using a moatherboard jumper

*  Can you explain some recent security breaches or well-known attacks .

* Security is fast moving field. How do you keep yourself updated.

* What is Cyber Kill Chain.
- A cyber kill chain reveals the phases of a cyberattack: from early reconnaissance to the goal of data exfiltration

* How can you classfy the roles in Cyber Security ? What is your understanding of different job roles and functions that are part of Cyber Security ?

* What is the CIA triad(or Triangle)?
- CIA triad, is a model designed to guide policies for information security within an organization. Confidentiality Integrity Availability

* What’s the difference between symmetric and asymmetric (public-key) cryptography?
- Symmetric encryption uses the same key to encrypt and decrypt data. In contrast, asymmetric encryption uses a pair of keys – a public key to encrypt data and a private key to decrypt information.

* What are Ports in Computers, how many ports a computer has ?
- A port is a virtual point where network connections start and end. Ports are software-based and managed by a computer's operating system. Each port is associated with a specific process or service. There are 65,535 port numbers, but not all are used every day. Restricted port numbers or well-known port numbers are reserved by prominent companies and range from 0 to 1023

* Why is deleted data not truly gone when you delete it?
- When you delete a file, the operating system marks the area where that data resides on the hard drive disk (HDD) as available, and logistically removes it from the file tree structure. The magnetic data still resides on the disk, but the pathway to accessing the data has been removed from the operating system

* What is AAA ?
- Authentication, Authorization, Accounting

* What is IAM and why it is used?
- Identity and access management (IAM) is a cybersecurity discipline focused on managing user identities and access permissions on a computer network. While IAM policies, processes, and technologies can differ between companies, the goal of any IAM initiative is to ensure that the right users and devices can access the right resources for the right reasons at the right time.

* How NMAP works
- It works by using IP packets to identify the hosts and IPs active on a network and then analyze these packets to provide information on each host and IP, as well as the operating systems they are running and what is running on each port.

* What is a Certification Authority (CA)
- It is an entity that stores, signs, and issues digital certificates.

* What port does ping work over?
- This question is a trap. Ping uses ICMP, so there are no real ports being used. ICMP sits on top of the IP address. It is not a layer four protocol. That means that you don't have to worry about assigning ports to a ping test.

* What is Salting (in context of Hashing), and why it is used ?
- It is adding additional random characters to the data to hash in order to strengthen it. This is most often done with passwords.Salting helps defend against attacks that use precomputed tables

* What is a hash function?
- A hash function is a deterministic procedure that takes an input (or “message”) and returns a string of characters of a fixed size—which is usually a “digest”—that is unique to the input.

* Would you Encrypt and Compress or Compress and Encrypt ? Why ?

Neither:
- Encrypting first and then compressing does not work.
  Compressing first can leak information about plaintext content through the ciphertext length


* What’s the difference between deep web and dark web?
- https://www.crowdstrike.com/wp-content/uploads/2020/12/dark-web-vs-deep-web-differences-1536x921.png

* What is the MITRE ATT&CK?
- The Adversarial Tactics, Techniques, and Common Knowledge or MITRE ATT&CK is a guideline for classifying and describing cyberattacks and intrusions.

* Explain/differentiate Vulnerability and Exploit
-  A vulnerability is a weakness in a system, network or application. Exploit : A tool used to take advantage of the vulnerability

* Explain the difference between Vulnerability, Threat and Risk.
- A vulnerability is a flaw or weakness in an asset’s design, implementation, or operation and management that could be exploited by a threat.
  A threat is a potential for a threat agent to exploit a vulnerability.
  A risk is the potential for loss when the threat happens.

* What is the difference between a Vulnerability Assessment and Penetration Test ?
- Vulnerability scanners alert companies to the preexisting flaws in their code and where they are located. Penetration tests attempt to exploit the vulnerabilities in a system to determine whether unauthorized access or other malicious activity is possible and identify which flaws pose a threat to the application.

* What is the difference Between Events, Alerts & Incidents ?
- An event is an observed change to the normal behavior of a system,
- An alert is a notification that a particular event (or series of events) has occurred
- An incident is an event that negatively affects the organization and impact business.

* What is an APT group ?
- An advanced persistent threat (APT) is a stealthy threat actor, typically a state or state-sponsored group, which gains unauthorized access to a computer network and remains undetected for an extended period.


* Any experience on working with any Ticketing tools ?

* What is the difference between authentication and authorization?
- Authentication means confirming your own identity, while authorization means granting access to the system. In simple terms, authentication is the process of verifying who you are, while authorization is the process of verifying what you have access to.

* What's the difference between encoding, encryption, and hashing?
- Encoding ensures message integrity. Can be easily reversible. Example: base64
     Encryption guarantees message confidentiality. Reversible only using the appropriate decryption keys. Example: AES256
     Hashing is a one-way function. Cannot be reversed. The output is fixed length and usually smaller than the input.
* What is a zero day?
- A 0-day is a vulnerability in a computer system that was previously unknown to its developers or anyone capable of mitigating it.

* What are HTTP response codes?
- HTTP response codes display whether a particular HTTP request has been completed.
    1xx (Informational) - The request has been received, and the process is continuing.
    2xx (Success) - The request was successfully received and accepted.
    3xx (Redirection) - Further action must be taken to complete it.
    4xx (Client Error) - Request cannot be fulfilled or has incorrect syntax.
    5xx (Server Error) - The server fails to fulfill the request.
