# Encryption and Authentication
  * What is this?  0b04b25a3e4a38d1ba2bda1bf74b97c2
  * Does TLS uses symmetric or asymmetric encryption?
    Both.
   * What is faster for encryption? using symmetric keys or asymmetric keys?
   * What is a three-way handshake?
   * How do cookies work?
   * How do sessions work?
   * Explain how OAuth works.
   * What is a public key infrastructure flow and how would I diagram it?
   * Describe the difference between synchronous and asynchronous encryption.
   * Describe SSL/TLS handshake.
   * How does HMAC work?
   * Why HMAC is designed in that way?
   * What is the difference between authentication vs authorization name spaces?
   * What’s the difference between Diffie-Hellman and RSA?
   * Describe how Diffie-Hellman works.

   * How does Kerberos work?
   * If you're going to compress and encrypt a file, which do you do first and why?
   * How do I authenticate you and know you sent the message?
   * Should you encrypt all data at rest?
   * What is Perfect Forward Secrecy?
   * Have you heard of 2FA ?  How 2FA protects users ? Is it possible to bypass 2FA with Phishing ?


