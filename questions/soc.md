# SOC

* What is LifeCycle of a Incident Response Process ?
* What is SLA ?
* I hope you understand the Idea of P0, P1.2.3.4 Incidents ? Which one will you handle with priority ?
* What is IOC (Indicators of Compromise) and IOA (Indicators of Attack) ?
* How can you say if an email is Phishing or not ?
* What will you do if user reports to have phishing email ?
* You discover user clicked links in phishing email, also shared credentials. What actions will be taken by you ?
* SPM DKIM DMARC records are related to ?
* How can you determine if the email spam ? what is the action taken to arrest the spread of same if you have to act ?
* make a playbook for case of BEC ( Business Email Compromise ).
* When a user reports their machine is hacked , what are the things yu look for ?
* What are some malware persistence Techniques ?
* What is Process Injection ? Name some (sub)methods.
* Which one is more acceptable Sypware or PUP ?
* What would you prefer on your system ? Rootkit or Backdoor ?
* Why Ransomware is a buzz word ?
* How can you detect/confirm that you (organisation) has been hit (affected) by ransomware ? What are the indicators ?
* How do you respond to a Ransomware attack ?
* Have you worked on any EDR Tools before ? What makes EDR different from Antivirus ?
* How/Why would you classify a website as malicious ?
* What is drive-by-downloads ?
* Can website with Green-Lock (SSL) be dangerous ?
* You discover your Infrastructure / Application is under DDoS attack ? What will be your resonse plan ?
* How would you advise backup policy of critical data in infrastructure ?
* What are some interesting logs you can collect in Windows Environment ?
* What are different DNS Records ? Explain.
* Explain DNS Exfiltration. How to detect DNS Exfiltration ?
* Browser, Application and OS are Vulnerable, which one will you priotize to fix and why ?
* How can you do Network Packet Analysis ? (Wireshark)
* Can you do do Network Packet Analysis with Wireshark ? What all information can you get from this analysis ?
* Can you do Network backet Analysis of HTTPS (SSL Enabled) traffic with Wireshark ?
* What are the logs from a Linux machine you would pick for SIEM ?
* What is SIEM ? Its Use ? ( More SIEM based questions in a small section later on same page)
* Describe some Incident that you faced, and how you handled it ?
* How do you Investigate a suspicious Login alert for a business user email?
* What is difference in Credential Stuffing ? and password Spraying ? How do you detect these ?
* Make a use-case of Password Spraying attack.
* What types of Malware Analysis are posible ?
* Explain Static Analysis and Dynamic Analysis of Malwares.
* What is the difference between an exploit and a vulnerability?
* Are you familiar with any of the tools used to scan for vulnerabilities? If no, can you name any?
* What is SCCM?
* How often are Microsoft patches released (non emergency?
* What is a Microsoft KB ?


